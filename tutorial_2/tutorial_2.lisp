; Napiště funkci ctvrty (LST) která vrátí čtvrtý prvek seznamu

(defun ctvrty (LST)
    (car (cdr (cdr (cdr LST))))
    ;(cadddr LST) - alternativni zapis
)

(print (ctvrty `(1 3 5 2 4)))

; Napište funkci delka (LST) která vrátí délku seznamu
(defun delka(LST)
    (if (null LST) 
        0
    (1+ (delka (cdr LST))))
)

(print (delka `(1 2 3 4 5 6 7)))

; Upravte funkci delka na koncovou rekurzi
(defun delka_koncova (LST)
    (delka_iter LST 0)
)

(defun delka_iter (LST ACC)
    (if (null LST) 
        ACC
    (delka_iter (cdr LST) (1+ ACC)))
)

(print (delka_koncova `(1 2 3 4 5 6 7)))

; Upravte funkci delka pro pocitani delky vcetne delky podseznamu (pocet atomu)
(defun delka_atom (LST)
    (delka_atom_iter LST 0)
)

(defun delka_atom_iter (LST ACC)
    (if (null LST)
        ACC
    (if (atom (car LST))
        (delka_atom_iter (cdr LST) (1+ ACC))
    (delka_atom_iter (cdr LST) (+ ACC (delka_atom (car LST))))))
)

(print (delka_atom `(1 (2 3) 4 (5 6 7))))

; Napiste funkci otoc (LST), ktera pomoci koncove rekurze otoci seznam
(defun otoc (LST)
    (otoc_iter LST NIL)
)

(defun otoc_iter (LST ACC)
    (if (null LST) ACC
    (otoc_iter (cdr LST) (cons (car LST) ACC)))
)

(print (otoc `(1 2 3 4 5 6 7)))

; Upravte funkci otoc aby otacela vcetne podseznamu
(defun otoc_podseznam (LST)
    (otoc_podseznam_iter LST NIL)
)

(defun otoc_podseznam_iter (LST ACC)
    (if (null LST) ACC
    (if (atom (car LST)) (otoc_podseznam_iter (cdr LST) (cons (car LST) ACC))
    (otoc_podseznam_iter (cdr LST) (cons (otoc_podseznam (car LST)) ACC))))
)

(print (otoc_podseznam `(1 2 (3 4 5) 6 7)))